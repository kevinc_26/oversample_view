import matplotlib.pyplot as plt
import numpy as np

if __name__ == "__main__":
    npz = np.load("./all_clean.npz")
    npz = {k: npz[k] for k in npz.files}
    samples = npz['samples']

    mapped = np.nonzero(samples[:, :, 0])

    minx, maxx = min(mapped[0]), max(mapped[0])
    miny, maxy = min(mapped[1]), max(mapped[1])

    from skimage.color import rgba2rgb, rgb2gray
    target_orig = rgb2gray(rgba2rgb(plt.imread('layout.png')))
    samples_orig = samples[:, :, :]

    samples = samples_orig[minx:maxx, miny:maxy, :]
    target = target_orig[minx:maxx, miny:maxy]

    void_inds = np.nonzero(target)
    plate_inds = np.nonzero(np.logical_not(target).astype(int))

    fig = plt.figure()
    ax_diff = fig.add_subplot(343)
    ax_diff.set_title('1st - Nth Sample')
    ax_orig = fig.add_subplot(342, sharex=ax_diff, sharey=ax_diff)
    ax_orig.set_title('Nth Sample')
    ax_max = fig.add_subplot(344, sharex=ax_diff, sharey=ax_diff)
    ax_max.set_title('Max Reading')
    ax_targ = fig.add_subplot(341, sharex=ax_diff, sharey=ax_diff)
    ax_targ.set_title('Original Target')
    ax_hist = fig.add_subplot(312)
    ax_evolve = fig.add_subplot(313)
    ax_evolve.set_xlabel('Sample data per pixel\nClick on top images to add a new line, click on this plot to clear')


    class IndexTracker(object):
        def __init__(self):
            self.title = 'Oversample Data'
            fig.suptitle('{}\n Use scroll wheel to set N'.format(self.title))

            self.ind = 1
            self.x_analyze = 0
            self.y_analyze = 0

            void_dose = np.ravel(samples[:, :, self.ind][void_inds[0], void_inds[1]])
            plate_dose = np.ravel(samples[:, :, self.ind][plate_inds[0], plate_inds[1]])

            self.im_diff = ax_diff.imshow(samples_orig[:, :, 0] - samples_orig[:, :, self.ind], cmap='inferno', vmin=0,
                                          vmax=100)
            self.im_orig = ax_orig.imshow(samples_orig[:, :, self.ind], cmap='inferno', vmin=0, vmax=100)
            self.im_max = ax_max.imshow(np.max(samples_orig, axis=2), cmap='inferno', vmin=0, vmax=100)
            self.im_targ = ax_targ.imshow(target_orig)
            self.hist = ax_hist.hist(void_dose, bins=50, color='red', alpha=0.5, edgecolor='black')
            self.hist = ax_hist.hist(plate_dose, bins=50, color='blue', alpha=0.5, edgecolor='black')
            self.evolve = ax_evolve.plot(np.arange(1, 129), samples_orig[self.x_analyze, self.y_analyze, :])
            self.update()

        def onscroll(self, event):
            num_slices = samples.shape[-1]
            if event.button == 'up':
                self.ind = (self.ind + 1) % num_slices
            else:
                self.ind = (self.ind - 1) % num_slices
            self.update()

        def onclick(self, event):
            # print(event.xdata, event.ydata)

            # for i,ax in enumerate([ax_diff, ax_orig, ax_max, ax_hist]):
            #     if ax == event.inaxes:
            #         print('Click in axes {}'.format(i+1))

            if event.inaxes in [ax_diff, ax_orig, ax_max, ax_targ]:
                self.x_analyze = int(event.xdata)
                self.y_analyze = int(event.ydata)
                ax_evolve.set_title('Sample evolution for point ({}, {})'.format(self.x_analyze, self.y_analyze))
                self.update()
            elif event.inaxes in [ax_evolve]:
                ax_evolve.cla()
                plt.draw()

        def update(self):
            self.im_diff.set_data(samples_orig[:, :, 0] - samples_orig[:, :, self.ind])
            self.im_orig.set_data(samples_orig[:, :, self.ind])

            void_dose = np.ravel(samples[:, :, self.ind][void_inds[0], void_inds[1]])
            plate_dose = np.ravel(samples[:, :, self.ind][plate_inds[0], plate_inds[1]])
            ax_hist.cla()
            ax_hist.hist(void_dose, bins=100, color='red', alpha=0.5, edgecolor='black', label='target')
            ax_hist.hist(plate_dose, bins=100, color='blue', alpha=0.5, edgecolor='black', label='void')
            self.hist = ax_hist.vlines([np.median(void_dose), np.median(plate_dose)], ymin=0, ymax=5000,
                                       colors=['r', 'b'])
            ax_hist.set_xlim(0, 250)
            ax_hist.set_ylim(0, 1500)
            ax_hist.legend()
            ax_hist.set_title('Target vs. Void Histogram')
            # print(self.evolve)
            if target_orig[self.y_analyze, self.x_analyze]:
                color_plt = 'b'
            else:
                color_plt = 'r'
            self.evolve = ax_evolve.plot(np.arange(1, 129), samples_orig[self.y_analyze, self.x_analyze, :], color_plt)
            # print(samples_orig[self.x_analyze, self.y_analyze, :])

            fig.suptitle('{}\nSlice: {}'.format(self.title, self.ind))

            plt.draw()


    tracker = IndexTracker()
    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    fig.canvas.mpl_connect('button_press_event', tracker.onclick)
    mng = plt.get_current_fig_manager()
    #mng.window.state("zoomed")
    plt.show(block=True)
